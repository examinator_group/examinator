package site.disturbingsoundsoff.examinator.presentation.teacher.test

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.VerticalAlignmentLine
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.R
import site.disturbingsoundsoff.examinator.data.Question
import site.disturbingsoundsoff.examinator.data.Test
import site.disturbingsoundsoff.examinator.presentation.components.CustomButton
import site.disturbingsoundsoff.examinator.presentation.components.SmallButton
import site.disturbingsoundsoff.examinator.presentation.components.WhiteCustomTextField
import site.disturbingsoundsoff.examinator.presentation.ui.theme.AccentPink
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme
import site.disturbingsoundsoff.examinator.presentation.ui.theme.UnfocusedGray
import site.disturbingsoundsoff.examinator.presentation.ui.theme.UnfocusedTextColor

@Composable
fun EditTestScreen(
    test: Test,
    currentIndex: Int,
    currentQuestion: Question,

    setTestTitle: (String) -> Unit,
    setTestSubject: (String) -> Unit,

    setCurrentIndex: (Int) -> Unit,

    addQuestion: () -> Unit,
    setQuestionBody: (String) -> Unit,
    setCorrectAnswer: (String) -> Unit,
    setQuestionScore: (Int) -> Unit,
    deleteQuestion: (Long) -> Unit,

    addAnswer: () -> Unit,
    setAnswer: (Int, String) -> Unit,
    makeQuestionOpenEnded: () -> Unit,
    deleteAnswer: (Int) -> Unit,
    deleteOpenEndedAnswer: () -> Unit,

    saveToDb: () -> Unit,
    goBack: () -> Unit
) {

    Scaffold(
        topBar = {
            EditTestTopBar(
                saveToDb = saveToDb,
                goBack = goBack,
                title = if (currentIndex < 0) "О тесте" else (currentIndex+1).toString()
            ) },
        bottomBar = { EditTestBottomBar(
            questions = test.questions,
            currentIndex = currentIndex,
            setCurrentIndex = setCurrentIndex
        )},
        floatingActionButton = {
            IconButton(
                modifier = Modifier
                    .height(54.dp)
                    .aspectRatio(1f)
                    .clip(RoundedCornerShape(16.dp))
                    .background(AccentPink),
                onClick = addQuestion
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    tint = Color.White,
                    contentDescription = ""
                )
            }
        }

    ) { paddings ->

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.primary)
                .padding(paddings)
                .padding(horizontal = 32.dp)
        ) {

            if (currentIndex < 0 ) {

                Spacer(modifier = Modifier.height(32.dp))
                TextArea(
                    label = "Название теста",
                    value = test.title,
                    onValueChange = setTestTitle
                )
                Spacer(modifier = Modifier.height(16.dp))
                TextArea(
                    label = "Название дисциплины",
                    value = test.subject,
                    onValueChange = setTestSubject
                )
            } else {
                QuestionAndScore(
                    setQuestionBody = setQuestionBody,
                    setQuestionScore = setQuestionScore,
                    deleteQuestion = deleteQuestion,
                    question = currentQuestion
                )

                Divider(
                    modifier = Modifier.padding(vertical = 16.dp),
                    color = UnfocusedGray
                )

                AnswerActions(
                    question = currentQuestion,
                    setCorrectAnswer = setCorrectAnswer,
                    deleteOpenEndedAnswer = deleteOpenEndedAnswer,
                    addAnswer = addAnswer,
                    makeQuestionOpenEnded = makeQuestionOpenEnded,
                    deleteAnswer = deleteAnswer,
                    setAnswer = setAnswer
                )
            }

        }

    }
}

@Composable
fun QuestionAndScore(
    setQuestionBody: (String) -> Unit,
    setQuestionScore: (Int) -> Unit,
    deleteQuestion: (Long) -> Unit,
    question: Question,
) {
    Row(
        modifier = Modifier.padding(top = 32.dp),
        verticalAlignment = Alignment.Top
    ) {

        Column(Modifier.weight(1f)){

            WhiteCustomTextField(
                modifier = Modifier
                    .fillMaxWidth(),
                value = question.body,
                onValueChange = setQuestionBody
            )
            Spacer(modifier = Modifier.height(16.dp))
            Row (
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceAround
            ){
                Text(text = "Баллы за верный ответ:")
                ScoreBox(
                    updateQuestionScore = setQuestionScore,
                    score = question.score
                )
            }

        }
        Column {
            SmallButton(onClick = {deleteQuestion(question.id)}) {
                Icon(
                    modifier = Modifier.size(26.dp),
                    painter = painterResource(id = R.drawable.cool_delete),
                    contentDescription = "удалить вопрос",
                    tint = Color.White
                )
            }
        }
    }

}

@Composable
fun AnswerActions(
    setAnswer: (Int, String) -> Unit,
    setCorrectAnswer: (String) -> Unit,
    deleteAnswer: (Int) -> Unit,
    deleteOpenEndedAnswer: () -> Unit,
    makeQuestionOpenEnded: () -> Unit,
    addAnswer: () -> Unit,
    question: Question,
) {

    when {

        question.openEnded -> {
            OpenEndedAnswer(
                correctAnswer = question.correctAnswer,
                setCorrectAnswer = setCorrectAnswer,
                delete = deleteOpenEndedAnswer
            )
        }

        !question.openEnded && question.answers.isNotEmpty() -> {
            CreatingAnswers(
                addAnswer = addAnswer,
                setAnswer = setAnswer,
                setCorrectAnswer = setCorrectAnswer,
                deleteAnswer = deleteAnswer ,
                correctAnswer = question.correctAnswer,
                answers = question.answers,
            )
        }

        else -> {
            ChooseAnswersButtons(
                addAnswer = addAnswer,
                makeQuestionOpenEnded = makeQuestionOpenEnded
            )
        }

    }
}

@Composable
fun CreatingAnswers(
    addAnswer: () -> Unit,
    setAnswer: (Int, String) -> Unit,
    setCorrectAnswer: (String) -> Unit,
    deleteAnswer: (Int) -> Unit,
    correctAnswer: String,
    answers: List<String>
) {
    Column (
        modifier = Modifier.verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ){

        answers.forEachIndexed { index, answer ->
            AnswerRow(
                setCorrectAnswer = setCorrectAnswer,
                setAnswer = {newAnswer -> setAnswer(index, newAnswer)},
                deleteAnswer = { deleteAnswer(index) },
                correctAnswer = correctAnswer,
                answer = answer
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        CustomButton(
            modifier = Modifier.padding(horizontal = 32.dp),
            text = "Добавить ответ",
            onClick = addAnswer,
            color = AccentPink,
            textColor = Color.White
        ){
            Icon(imageVector = Icons.Rounded.Add, contentDescription = "Добавить ответ", tint = Color.White)
        }
        Spacer(modifier = Modifier.height(64.dp))
    }
}

@Composable
fun AnswerRow(
    setAnswer: (String) -> Unit,
    setCorrectAnswer: (String) -> Unit,
    deleteAnswer: () -> Unit,
    correctAnswer: String,
    answer: String
) {
    Row {
        RadioButton(
            selected = answer == correctAnswer,
            onClick = { setCorrectAnswer(answer) },
            colors = RadioButtonDefaults.colors(
                selectedColor = Color.White,
                unselectedColor = Color.White
            )
        )
        WhiteCustomTextField(modifier = Modifier.weight(1f), value = answer, onValueChange = setAnswer)

        SmallButton(onClick = deleteAnswer) {
            Icon(
                modifier = Modifier.size(26.dp),
                painter = painterResource(id = R.drawable.cool_delete),
                contentDescription = "удалить вопрос",
                tint = Color.White
            )
        }

    }
}

@Composable
fun ScoreBox(
    updateQuestionScore: (Int) -> Unit,
    score: Int,
) {

    Box(modifier = Modifier
        .size(42.dp)
        .clip(RoundedCornerShape(6.dp))
        .border(width = 1.dp, color = Color.Black, shape = RoundedCornerShape(6.dp))
        .background(color = Color.White),
        contentAlignment = Alignment.Center
    ) {
        BasicTextField(
            value = score.toString(),
            onValueChange = {
                var newScore = it
                if (newScore.length > 2){
                    newScore = newScore.substring(1,3)
                }
                updateQuestionScore(newScore.toIntOrNull() ?: 0)
            },
            textStyle = MaterialTheme.typography.bodyLarge.copy(
                textAlign = TextAlign.Center
            )
        )
    }

}


@Composable
fun OpenEndedAnswer(
    correctAnswer: String,
    setCorrectAnswer: (String) -> Unit,
    delete: () -> Unit
) {
    Row(
        modifier = Modifier.padding(top = 32.dp),
        verticalAlignment = Alignment.Top
    ) {

        WhiteCustomTextField(
            modifier = Modifier.weight(1f),
            value = correctAnswer,
            onValueChange = setCorrectAnswer
        )
        SmallButton(onClick = delete) {
            Icon(
                modifier = Modifier.size(26.dp),
                painter = painterResource(id = R.drawable.cool_delete),
                contentDescription = "удалить вопрос",
                tint = Color.White
            )
        }
    }
}

@Composable
fun ChooseAnswersButtons(
    makeQuestionOpenEnded: () -> Unit,
    addAnswer: () -> Unit
) {
    Column(
        Modifier.padding(horizontal = 28.dp),
    ) {
        CustomButton(
            text = "Произвольный ответ",
            onClick = makeQuestionOpenEnded,
            color = AccentPink,
            textColor = Color.White
        ) {
            Icon(
                imageVector = Icons.Rounded.Add,
                contentDescription = "Добавить произвольный ответ",
                tint = Color.White
            )
        }
        CustomButton(
            text = "Варианты ответа",
            onClick = addAnswer,
            color = AccentPink,
            textColor = Color.White
        ) {
            Icon(
                imageVector = Icons.Rounded.Add,
                contentDescription = "Добавить произвольный ответ",
                tint = Color.White
            )
        }
    }
}

@Composable
fun TextArea(
    label: String,
    value: String,
    onValueChange: (String) -> Unit,
    keyboardOptions: KeyboardOptions = KeyboardOptions(imeAction = ImeAction.Next)
) {
    Text(
        text = label,
        modifier = Modifier.padding(start = 20.dp, bottom = 10.dp),
        color = UnfocusedTextColor,
    )
    WhiteCustomTextField(
        modifier = Modifier.fillMaxWidth(),
        value = value,
        onValueChange = onValueChange,
        keyboardOptions = keyboardOptions
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditTestTopBar(
    title: String,
    saveToDb: () -> Unit,
    goBack: () -> Unit
) {
    val context = LocalContext.current
    TopAppBar(
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = MaterialTheme.colorScheme.secondary
        ),
        title = {
            Row (verticalAlignment = Alignment.CenterVertically){
                Text(text = title)
                Spacer(modifier = Modifier.weight(1f))
                Icon(
                    painter = painterResource(id = R.drawable.save),
                    contentDescription = "save",
                    modifier = Modifier
                        .size(42.dp)
                        .padding(end = 8.dp)
                        .clickable {
                            saveToDb()
                            Toast.makeText(context, "Сохранено", Toast.LENGTH_SHORT).show()
                        }
                )
//                Text(
//                    text = "Сохранить",
//                    modifier = Modifier.clickable {
//                        saveToDb()
//                        Toast.makeText(context, "Сохранено", Toast.LENGTH_SHORT).show()
//                    }
//                )
            }
        },
        navigationIcon = {
            IconButton(
                onClick = goBack
            ) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    tint = Color.White,
                    contentDescription = "вернуться на прошлый экран",
                )
            }
        },
    )
}

@Composable
fun EditTestBottomBar(
    currentIndex: Int,
    setCurrentIndex: (Int) -> Unit,
    questions: List<Question>,
    modifier: Modifier = Modifier,
) {

    Row(
        modifier = modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.secondary)
            .padding(bottom = 32.dp, top = 8.dp)
    ) {
        LazyRow {
            item {
                NavBottomIcon(
                    modifier = Modifier
                        .clickable {
                            if (currentIndex != -1) {
                                setCurrentIndex(-1)
                            }
                        }
                        .padding(start = 32.dp),
                    selected = currentIndex < 0
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.file),
                        tint = if (currentIndex < 0) Color.White else AccentPink,
                        contentDescription = "настройки теста",
                    )
                }
            }

            items(items = questions, key = {questions.indexOf(it)}) {question ->
                val itemIndex = questions.indexOf(question)
                NavBottomIcon(
                    modifier = Modifier.clickable {
                        if (currentIndex != itemIndex) { setCurrentIndex(itemIndex) }
                    },
                    selected = itemIndex == currentIndex,
                ){
                    Text(
                        modifier = Modifier.padding(horizontal = 6.dp),
                        text = (itemIndex + 1).toString(),
                        style = TextStyle(
                            color = if (itemIndex == currentIndex) Color.White
                            else AccentPink
                        )
                    )
                }
            }
            item {
                Spacer(modifier = Modifier.width(32.dp))
            }
        }
    }
}


@Composable
fun NavBottomIcon(
    modifier: Modifier = Modifier,
    selected: Boolean = false,
    content: @Composable () -> Unit
) {
    Box(
        modifier = modifier
            .height(66.dp)
            .aspectRatio(1f)
            .padding(vertical = 6.dp, horizontal = 8.dp)
            .clip(RoundedCornerShape(16.dp))
            .background(
                if (selected) AccentPink
                else Color.White
            ),
        contentAlignment = Alignment.Center
    ) {
        content()
    }
}

@Preview
@Composable
private fun CreateTestScreenPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        EditTestScreen(
            test = Test(id = 1, author = "me", teacherId = 1),
            setTestTitle = {},
            currentIndex = -1,
            currentQuestion = Question(
                id = 1,
                body = "question1",
                score = 1,
                correctAnswer = "true",
                answers = emptyList(),
            ),
            setCurrentIndex = {},
            setQuestionScore = {},
            deleteQuestion = {},
            deleteOpenEndedAnswer = {},
            setCorrectAnswer = {},
            setTestSubject = {},
            saveToDb = {},
            goBack = {},
            addQuestion = {},
            setQuestionBody = {},
            makeQuestionOpenEnded = {},
            addAnswer = {},
            setAnswer = {a,b -> },
            deleteAnswer = {}

        )
    }
}

@Preview
@Composable
private fun PreviewQuestion() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        EditTestScreen(
            test = Test(id = 1, author = "me", teacherId = 1),
            setTestTitle = {},
            currentIndex = 1,
            currentQuestion = Question(
                id = 1,
                body = "question1",
                score = 1,
                correctAnswer = "true",
                answers = emptyList(),
            ),
            setCurrentIndex = {},
            setAnswer = {a,b -> },
            deleteAnswer = {},
            setQuestionScore = {},
            deleteQuestion = {},
            deleteOpenEndedAnswer = {},
            setCorrectAnswer = {},
            setTestSubject = {},
            makeQuestionOpenEnded = {},
            saveToDb = {},
            goBack = {},
            setQuestionBody = {},
            addQuestion = {},
            addAnswer = {}
        )
    }
}

@Preview
@Composable
private fun PreviewOpenEndedQuestion() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        EditTestScreen(
            test = Test(id = 1, author = "me", teacherId = 1),
            setTestTitle = {},
            setAnswer = {a,b -> },
            deleteAnswer = {},
            currentIndex = 1,
            currentQuestion = Question(
                id = 1,
                body = "open ended question",
                score = 1,
                correctAnswer = "example on open ended answer",
                answers = emptyList(),
                openEnded = true,
            ),
            setCurrentIndex = {},
            setQuestionScore = {},
            deleteQuestion = {},
            deleteOpenEndedAnswer = {},
            setCorrectAnswer = {},
            setTestSubject = {},
            makeQuestionOpenEnded = {},
            saveToDb = {},
            goBack = {},
            addQuestion = {},
            setQuestionBody = {},
            addAnswer = {}
        )
    }
}

@Preview
@Composable
private fun OpenEndedAnswerPrev() {
    OpenEndedAnswer(
        "ansdwer",
        {},
        {}
    )
}

@Preview
@Composable
private fun QuestionAndScorePrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        QuestionAndScore(
            question = Question(id = 1, body = "test"),
            setQuestionBody = {},
            setQuestionScore = {},
            deleteQuestion = {}
        )
    }

}
