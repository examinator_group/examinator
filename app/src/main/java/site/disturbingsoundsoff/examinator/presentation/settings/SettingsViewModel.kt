package site.disturbingsoundsoff.examinator.presentation.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import site.disturbingsoundsoff.examinator.common.UserTypes
import site.disturbingsoundsoff.examinator.data.UserDatastoreRepository

class SettingsViewModel(
    private val dataStore: UserDatastoreRepository
) : ViewModel() {
    private val _userType = MutableStateFlow("")
    val userType = _userType.asStateFlow()

//    val userType = dataStore.userType.stateIn(
//        scope = viewModelScope,
//        started = SharingStarted.WhileSubscribed(5_000L),
//        initialValue = UserTypes.NEEDS_LOGIN
//    )
    init {
        viewModelScope.launch {
            _userType.update {
                dataStore.userType.first()
            }
        }
    }


    fun setUserType(newType: String) {
        if (newType !in UserTypes) {
            throw IllegalArgumentException("unexisting user type")
        }
        viewModelScope.launch {
            dataStore.setUserType(newType)
            _userType.update { newType }
        }

    }
}