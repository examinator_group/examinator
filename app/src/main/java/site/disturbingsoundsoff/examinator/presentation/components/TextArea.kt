package site.disturbingsoundsoff.examinator.presentation.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.presentation.ui.theme.UnfocusedTextColor

@Composable
fun TextArea(
    label: String,
    value: String,
    onValueChange: (String) -> Unit,
    keyboardOptions: KeyboardOptions = KeyboardOptions(imeAction = ImeAction.Next)
) {
    Text(
        text = label,
        modifier = Modifier.padding(start = 20.dp, bottom = 10.dp),
        color = UnfocusedTextColor,
    )
    CustomTextField(
        modifier = Modifier.fillMaxWidth(),
        value = value,
        onValueChange = onValueChange,
        keyboardOptions = keyboardOptions
    )

}

@Composable
fun TestTestTest(
    label: String,
    value: String,
    onValueChange: () -> String,
) {
    Text(
        text = label,
        modifier = Modifier.padding(top = 21.dp)
    )

    CustomTextField(
       value = value,
       onValueChange = {_ -> onValueChange()}
    )
}

