package site.disturbingsoundsoff.examinator.presentation

import android.widget.Toast
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.navigation.navigation
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf
import site.disturbingsoundsoff.examinator.common.UserTypes
import site.disturbingsoundsoff.examinator.presentation.login.UserDataViewModel
import site.disturbingsoundsoff.examinator.presentation.login.UserInfoInputScreen
import site.disturbingsoundsoff.examinator.presentation.login.UserTypeSelectionScreen
import site.disturbingsoundsoff.examinator.presentation.login.UserTypeViewModel
import site.disturbingsoundsoff.examinator.presentation.settings.SettingsScreen
import site.disturbingsoundsoff.examinator.presentation.settings.SettingsViewModel
import site.disturbingsoundsoff.examinator.presentation.student.StudentHomeScreen
import site.disturbingsoundsoff.examinator.presentation.student.TestScreen
import site.disturbingsoundsoff.examinator.presentation.student.TestViewModel
import site.disturbingsoundsoff.examinator.presentation.teacher.TeacherHomeScreen
import site.disturbingsoundsoff.examinator.presentation.teacher.test.EditTestScreen
import site.disturbingsoundsoff.examinator.presentation.teacher.test.EditTestViewModel
import site.disturbingsoundsoff.examinator.presentation.teacher.test.ListTestScreen
import site.disturbingsoundsoff.examinator.presentation.teacher.test.ListTestViewModel
import site.disturbingsoundsoff.examinator.presentation.teacher.test.TestActionScreen
import site.disturbingsoundsoff.examinator.presentation.teacher.test.TestActionViewModel
private const val TAG = "AppNavigation"
@Composable
fun AppNavigation(
    userTypeDS: String,
    fileContents: String = "",
    fileSupported: Boolean = false,
    fileForStudent: Boolean = false,
    launchFilePicker: () -> Unit = {},
    navController: NavHostController = rememberNavController()
) {
    var userType by remember { mutableStateOf(userTypeDS) }
//    val navController: NavHostController = rememberNavController()

    NavHost(navController = navController, startDestination = userType) {

        navigation(
            startDestination = "user_type_selection",
            route = UserTypes.NEEDS_LOGIN
        ) {

            composable(
                route = "user_type_selection",
                enterTransition = { EnterTransition.None },
                exitTransition = { ExitTransition.None },
            ) {

                val vm: UserTypeViewModel = getViewModel()

                UserTypeSelectionScreen(
                    setUserType = vm::setUserType,
                    goToNext = { navController.navigate("user_info_input") }
                )
            }
            composable(
                route = "user_info_input",
                enterTransition = { EnterTransition.None },
                exitTransition = { ExitTransition.None },
            ) {

                val vm: UserDataViewModel = getViewModel()
                val loginData by vm.loginData.collectAsState()

                UserInfoInputScreen(
                    userType = loginData.userType,
                    name = loginData.name,
                    onNameChange = vm::setName,
                    surname = loginData.surname,
                    onSurnameChange = vm::setSurname,
                    middleName = loginData.middleName,
                    onMiddleNameChange = vm::setMiddlename,
                    position = loginData.position,
                    onPositionChanged = vm::setPosition,
                    backToUserType = { navController.popBackStack() },
                    goToHomeScreen = { userType ->
                        navController.popBackStack(route = UserTypes.NEEDS_LOGIN, inclusive = true)
                        navController.navigate(userType)
                    }
                )
            }
        }

        navigation(
            route = UserTypes.TEACHER,
//            startDestination = if (fileContents == "") "teacher_home" else "edit_test/-1"
            startDestination = "teacher_home"
        ) {

            composable(
                route = "teacher_home",
                enterTransition = { EnterTransition.None },
                exitTransition = { ExitTransition.None },
            ) {

                if (fileContents.isNotEmpty()) {
                    if (!fileSupported or fileForStudent) {
                        navController.navigate("error_screen")
                    }
                    else {
                        navController.navigate("edit_test/-1")
                    }
                }


                TeacherHomeScreen(
                    viewTests = { navController.navigate("list_tests") },
                    createTest = { navController.navigate("edit_test/-1") },
                    goToSettings = { navController.navigate("settings") },
                    launchFilePicker = launchFilePicker
                )
            }

            composable(
                route = "list_tests",
                enterTransition = { EnterTransition.None },
                exitTransition = { ExitTransition.None },
            ) {

                val vm: ListTestViewModel = getViewModel()
                val tests by vm.tests.collectAsState()

                ListTestScreen(
                    tests = tests,
//                    goToEditTest = {id -> navController.navigate("edit_test/$id")},
                    goToTestActions = {id -> navController.navigate("test_actions/$id")},
                    goBack = { navController.popBackStack() },

                )
            }


            composable(
                route = "test_actions/{id}",
                arguments = listOf(
                    navArgument("id") {type = NavType.LongType}
                ),
                enterTransition = { EnterTransition.None },
                exitTransition = { ExitTransition.None },
            ) {backStack ->
                val context = LocalContext.current
                val id: Long = backStack.arguments?.getLong("id") ?: -1
                val filesDir = context.filesDir

                val vm: TestActionViewModel = getViewModel(
                    parameters =  {
                        parametersOf(id, filesDir)
                    }
                )
//                val testFile by vm.testFile.collectAsState()
                val studentTestFile by vm.studentTestFile.collectAsState()
                val exportTestFile by vm.exportTestFile.collectAsState()

                TestActionScreen (
                    studentTestFile = studentTestFile,
                    exportTestFile = exportTestFile,
                    editTest = { navController.navigate("edit_test/$id") },
                    goBack = { navController.popBackStack() },
                    deleteTest = {
                        vm.deleteTest()
                        Toast.makeText(context, "Тест удален", Toast.LENGTH_SHORT).show()
                        navController.popBackStack()
                    }
                )
            }

            composable(
                route = "edit_test/{id}",
                arguments = listOf(
                    navArgument("id") {type = NavType.LongType}
                ),
                enterTransition = { EnterTransition.None },
                exitTransition = { ExitTransition.None },
            ) {backStack ->
                val id: Long = backStack.arguments?.getLong("id") ?: -1

                val vm: EditTestViewModel = getViewModel(
                    parameters =  { parametersOf(id) }
                )
                if (fileContents != "") {
                    vm.parseFileData(fileContents)
                }

                val test by vm.test.collectAsState()
                val currentIndex by vm.currentIndex.collectAsState()

                EditTestScreen(
                    test = test,
                    currentIndex = currentIndex,
                    currentQuestion = vm.question,

                    setTestTitle = vm::onTitleChanged,
                    setTestSubject = vm::onSubjectChanged,

                    setCurrentIndex = vm::setCurrentIndex,

                    addQuestion = vm::addQuestion,
                    setQuestionBody = vm::setQuestionBody,
                    setQuestionScore = vm::setScore,
                    makeQuestionOpenEnded = vm::makeQuestionOpenEnded,
                    deleteQuestion = vm::deleteQuestion,

                    addAnswer = vm::addAnswer,
                    setAnswer = vm::setAnswer,
                    setCorrectAnswer = vm::setCorrectAnswer,
                    deleteAnswer = vm::deleteAnswer,
                    deleteOpenEndedAnswer = vm::deleteOpenEndedAnswer,

                    saveToDb = vm::saveTest,
                    goBack = {
                        val popped = navController.popBackStack()
                        if (!popped) {
                            navController.popBackStack(route = "edit_test/-1", inclusive = true)
                            navController.navigate("teacher_home")
                        }
                    },
                )


            }

//            composable(
//                route = "file_opened",
//                enterTransition = { EnterTransition.None },
//                exitTransition = { ExitTransition.None },
//            ) {
//
//            }
        }

        navigation(
            route = UserTypes.STUDENT,
            startDestination = "student_home"
        ) {

            composable(
                route = "student_home",
                enterTransition = { EnterTransition.None },
                exitTransition = { ExitTransition.None },
            ) {
                val context = LocalContext.current
                StudentHomeScreen(
                    goToSettings = { navController.navigate("settings") },
//                    goToTest = { testUri -> navController.navigate("test/$testUri") },
                    goToTest = { fileData ->
                        /* TODO: this is shit
                                i should use this instead https://stackoverflow.com/questions/67121433/how-to-pass-object-in-navigation-in-jetpack-compose#:~:text=2.%20passing%20the%20object%20using%20navbackstackentry
                                apparently this is also bad
                                idk what to do then
                                will tackle this problem later
                         */
                        navController.currentBackStackEntry?.savedStateHandle?.set("fileData", fileData)
//                        navController.navigate("test/${fileData.replace("?", "@|@")}")
                        navController.navigate("test/PTEST")
                    },
                    launchFilePicker = launchFilePicker
                )
            }

            composable(
                route = "test/{fileData}",
                arguments = listOf(
                    navArgument("fileData") {type = NavType.StringType}
                ),
                enterTransition = { EnterTransition.None },
                exitTransition = { ExitTransition.None }
            ) { backStack ->

//                val fileData: String = backStack.arguments?.getString("fileData")?.replace("@|@", "?") ?: ""
                val fileData: String = navController.previousBackStackEntry?.savedStateHandle?.get("fileData") ?: ""

                val vm: TestViewModel = getViewModel(
                    parameters = {
                        parametersOf(fileData)
                    }
                )

                val test by vm.test.collectAsState()
                val questions by vm.questions.collectAsState()
                val currentIndex by vm.currentIndex.collectAsState()


                TestScreen(
                    test = test,
                    saveResultToDb = {},
                    questions = questions,
                    currentIndex = currentIndex,
                    setCurrentIndex = vm::setCurrentIndex,
                    goBack = { navController.popBackStack() }
                )

            }
        }

        composable(
            route = "settings",
            enterTransition = { EnterTransition.None },
            exitTransition = { ExitTransition.None },
        ) {
            val vm: SettingsViewModel = getViewModel()
            val userTypeState by vm.userType.collectAsState()
            SettingsScreen(
                userType = userTypeState,
                setUserType = { newValue ->
                    vm.setUserType(newValue)
//                    userType = newValue
                },
                goToHomeScreen = {
//                    navController.navigate(userType) {
//                        popUpTo(navController.graph.startDestinationId) { inclusive = true }
//                    }.also {
//                        userType = userTypeState
//                    }
                    // TODO: this is also some bullshit solution, i'm afraid to think what it does to backstack
                        userType = userTypeState
                }
            )
        }

    }
}
