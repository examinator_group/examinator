package site.disturbingsoundsoff.examinator.presentation.login

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.R
import site.disturbingsoundsoff.examinator.common.UserTypes
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme

@Composable
fun UserTypeSelectionScreen(
    setUserType: (String) -> Unit,
    goToNext: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colorScheme.primary),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Кто вы ?",
            style = MaterialTheme.typography.displaySmall.copy(color = Color.White)
        )
        Spacer(Modifier.size(64.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Icon(
                    painter = painterResource(id = R.drawable.student),
                    contentDescription = "Обучающийся",
                    tint = Color.White
                )
                Spacer(Modifier.size(32.dp))
                Button(
                    onClick = {
                        setUserType(UserTypes.STUDENT)
                        goToNext()
                    },
                    colors = ButtonDefaults.buttonColors(containerColor = Color.White),
                    shape = RoundedCornerShape(16.dp)
                ) {
                    Text(text = "Обучающийся", color = MaterialTheme.colorScheme.primary)
                }
            }
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Icon(
                    painter = painterResource(id = R.drawable.teacher),
                    contentDescription = "Преподаватель",
                    tint = Color.White
                )
                Spacer(Modifier.size(32.dp))
                Button(
                    onClick = {
                        setUserType(UserTypes.TEACHER)
                        goToNext()
                    },
                    colors = ButtonDefaults.buttonColors(containerColor = Color.White),
                    shape = RoundedCornerShape(16.dp)
                ) {
                    Text(text = "Преподаватель", color = MaterialTheme.colorScheme.primary)
                }
            }
        }
        Spacer(modifier = Modifier.size(64.dp))
    }
}

@Preview(showSystemUi = true)
@Composable
fun UserTypeSelectionPreview() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        UserTypeSelectionScreen({}, {})
    }
}
