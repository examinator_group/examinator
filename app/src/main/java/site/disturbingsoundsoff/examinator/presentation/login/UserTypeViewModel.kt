package site.disturbingsoundsoff.examinator.presentation.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import site.disturbingsoundsoff.examinator.common.UserTypes
import site.disturbingsoundsoff.examinator.data.UserDatastoreRepository

class UserTypeViewModel(
    private val userDatastoreRepository: UserDatastoreRepository
) : ViewModel() {

    val userType = userDatastoreRepository.userType.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000L),
        initialValue = UserTypes.NEEDS_LOGIN
    )

    fun setUserType(type: String) {
        if (type !in UserTypes) {
            throw IllegalArgumentException("unexisting user type")
        }
        viewModelScope.launch {
            userDatastoreRepository.setUserType(type)
        }
    }

}
