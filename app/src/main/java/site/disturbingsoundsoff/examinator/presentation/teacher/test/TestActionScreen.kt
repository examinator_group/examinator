package site.disturbingsoundsoff.examinator.presentation.teacher.test

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.FileProvider
import site.disturbingsoundsoff.examinator.R
import site.disturbingsoundsoff.examinator.presentation.components.CustomButton
import site.disturbingsoundsoff.examinator.presentation.ui.theme.DangerRed
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme
import site.disturbingsoundsoff.examinator.presentation.ui.theme.UnfocusedGray
import java.io.File

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TestActionScreen(
    editTest: () -> Unit,
    goBack: () -> Unit,
    deleteTest: () -> Unit,
    studentTestFile: File,
    exportTestFile: File
) {

    val context = LocalContext.current

    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.secondary),
                title = {
                    Text(
                        text = "Действия",
                        // TODO: if white color is not set, text actually is light gray
                        color = Color.White
                    )
                },
                navigationIcon = {
                    IconButton(
                        onClick = goBack
                    ) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            tint = Color.White,
                            contentDescription = "вернуться на прошлый экран",
                        )
                    }
                },
            )
        }
    ) { paddings ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colorScheme.primary)
                .padding(paddings)
                .verticalScroll(rememberScrollState())
        ) {

            Spacer(modifier = Modifier.height(16.dp))

            Column(
                modifier = Modifier.padding(horizontal = 32.dp),
                verticalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                CustomButton(
                    text = "Редактировать тест",
                    onClick = editTest
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.edit),
                        contentDescription = "Редактировать тест",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                TextDivider(text = "Раздать тест")

                CustomButton(
                    text = "Файлом",
                    onClick = {

//                        val fileUri = FileProvider.getUriForFile(context, "${context.packageName}.fileprovider", studentTest)
//
//                        val sendIntent = Intent().apply {
//                            action = Intent.ACTION_SEND
//                            type = "application/json"
//                            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
//                            putExtra(Intent.EXTRA_STREAM, fileUri)
//                        }
//
//                        val shareIntent = Intent.createChooser(sendIntent, "ПРОВЕРКА НАЗВАНИЯ !!!")
//                        context.startActivity(shareIntent)
                        shareFile(context = context, file = studentTestFile)
                    }

                )
//                CustomButton(
//                    text = "Bluetooth",
//                    onClick = { /* TODO */ }
//                )
                CustomButton(
                    text = "На сервер",
                    onClick = { /* TODO */ }
                )

                TextDivider(text = "Принять результаты")

                CustomButton(
                    text = "Файлом",
                    onClick = { /* TODO */ }
                )
//                CustomButton(
//                    text = "Bluetooth",
//                    onClick = { /* TODO */ }
//                )
                CustomButton(
                    text = "С сервера",
                    onClick = { /* TODO */ }
                )

                Divider(
                    modifier = Modifier.clip(CircleShape),
                    color = UnfocusedGray,
                )

                CustomButton(
                    text = "Посмотреть результаты",
                    onClick = { /* TODO */ }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.eye),
                        contentDescription = "Посмотреть результаты",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                CustomButton(
                    text = "Экспортировать",
                    onClick = { /* TODO */
                        shareFile(context = context, file = exportTestFile)
                    }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.export),
                        contentDescription = "Экспортировать",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                CustomButton(
                    text = "Удалить",
                    color = DangerRed,
                    textColor = Color.White,
                    onClick = deleteTest
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.cool_delete),
                        contentDescription = "Удалить",
                        tint = Color.White
                    )
                }

            }
        }
    }
}

fun shareFile(context: Context, file: File){
    /* TODO */

    val fileUri = FileProvider.getUriForFile(context, "${context.packageName}.fileprovider", file)

    val sendIntent = Intent().apply {
        action = Intent.ACTION_SEND
        type = "application/json"
        flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        putExtra(Intent.EXTRA_STREAM, fileUri)
    }

    // TODO: find where title is used and change it
    val shareIntent = Intent.createChooser(sendIntent, "ПРОВЕРКА НАЗВАНИЯ!!!")
    context.startActivity(shareIntent)
}

@Composable
fun TextDivider(text: String) {
    Row(
        modifier = Modifier.padding(vertical = 8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            modifier = Modifier.padding(end = 16.dp),
            text = text,
            fontSize = 12.sp
        )
        Divider(
            modifier = Modifier.clip(CircleShape),
            color = UnfocusedGray,
        )
    }

}

@Preview(showSystemUi = true)
@Composable
private fun TeacherHomeScreenPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        TestActionScreen(
            goBack = {},
            editTest = {},
            deleteTest = {},
            studentTestFile = File(""),
            exportTestFile = File(""),

        )
    }
}


