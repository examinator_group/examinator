package site.disturbingsoundsoff.examinator.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val PurpleDark = Color(0xFF2D2155)
val PurpleMain = Color(0xFF4E3A94)
val PurpleLight = Color(0xFF8375B4)
val AccentPink = Color(0xFFB747DF)
val UnfocusedGray = Color(0xFFE0DDEC)
val UnfocusedTextColor = Color(0xFFCAC4DF)
val DangerRed = Color(0xFFFF5555)