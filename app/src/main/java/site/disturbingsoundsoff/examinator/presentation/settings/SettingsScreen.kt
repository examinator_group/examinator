package site.disturbingsoundsoff.examinator.presentation.settings

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountBox
import androidx.compose.material.icons.rounded.ArrowDropDown
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import site.disturbingsoundsoff.examinator.common.UserTypes
import site.disturbingsoundsoff.examinator.presentation.components.CustomButton
import site.disturbingsoundsoff.examinator.presentation.student.StudentHomeScreen
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsScreen(
    userType: String,
    setUserType: (String) -> Unit,
    goToHomeScreen: () -> Unit
) {
    BackHandler(onBack = goToHomeScreen)

    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.secondary),
                title = {
                    Text(
                        modifier = Modifier.padding(start = 40.dp),
                        text = "Настройки",
                        color = Color.White
                    )
                },
            )

        }
    ) { paddings ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colorScheme.primary)
                .padding(paddings)
                .padding(horizontal = 32.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {

            var expanded by remember { mutableStateOf(false) }
            val allowedUserTypes = listOf(UserTypes.TEACHER, UserTypes.STUDENT)

            Column {

                Text(
                    modifier = Modifier.padding(start = 8.dp),
                    text = "Тип пользователя:",
                )

                Button(
                    modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(15.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = Color.White),
                    onClick = { expanded = !expanded },
                ) {
                    Text(modifier = Modifier.weight(1f), text = userType)
                    Icon(
                        imageVector = Icons.Rounded.ArrowDropDown,
                        contentDescription = "Drop Down",
                    )
                }

                DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
                    allowedUserTypes.forEach {
                        DropdownMenuItem(
                            onClick = {
                                setUserType(it)
                                expanded = false
                            },
                            text = {
                                Text(text = it)
                            }
                        )
                    }
                }
            }


        }
    }
}

@Preview
@Composable
private fun SettingsScreenPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        SettingsScreen(
            userType = "Teacher",
            setUserType = {},
            goToHomeScreen = {}
        )
    }
}