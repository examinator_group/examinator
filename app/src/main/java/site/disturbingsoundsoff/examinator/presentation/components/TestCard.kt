package site.disturbingsoundsoff.examinator.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.R
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme

@Composable
fun TestCard(
    modifier: Modifier = Modifier,
    title: String,
    subject: String,
    favourite: Boolean
) {
    Card (
        modifier = modifier
            .fillMaxWidth()
    ){
        Text(
            modifier = Modifier.padding(start = 16.dp, end = 40.dp, top = 16.dp),
            text = title,
            style = MaterialTheme.typography.titleSmall
        )
        Text(
            modifier = Modifier.padding(start = 16.dp, end = 40.dp, top = 16.dp),
            text = subject
        )
        Icon(
            modifier = Modifier
                .align(Alignment.End)
                .padding(end = 16.dp, bottom = 16.dp),
            painter = if (favourite) {
                painterResource(id = R.drawable.star_filled)
            } else {
                painterResource(id = R.drawable.star_outlined)
            },
            tint = MaterialTheme.colorScheme.primary,
            contentDescription = "favourite"
        )
    }
}

@Preview
@Composable
private fun TestCardNotFavPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        TestCard(
            title ="Основные законодательные акты Российской Федерации",
            subject ="Основы безопасности жизнедеятельности",
            favourite = false
        )
    }
}

@Preview
@Composable
private fun TestCardFavPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        TestCard(
            title ="Основные законодательные акты Российской Федерации",
            subject ="Основы безопасности жизнедеятельности",
            favourite = true
        )
    }
}
