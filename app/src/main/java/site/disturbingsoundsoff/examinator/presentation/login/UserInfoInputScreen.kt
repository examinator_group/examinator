package site.disturbingsoundsoff.examinator.presentation.login

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.rounded.KeyboardArrowRight
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import site.disturbingsoundsoff.examinator.common.UserTypes
import site.disturbingsoundsoff.examinator.presentation.components.CustomTextField
import site.disturbingsoundsoff.examinator.presentation.components.TextArea
import site.disturbingsoundsoff.examinator.presentation.ui.theme.AccentPink
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme
import site.disturbingsoundsoff.examinator.presentation.ui.theme.UnfocusedTextColor

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UserInfoInputScreen(
    userType: String,
    name: String,
    onNameChange: (String) -> Unit,
    surname: String,
    onSurnameChange: (String) -> Unit,
    middleName: String,
    onMiddleNameChange: (String) -> Unit,
    position: String,
    onPositionChanged: (String) -> Unit,
    backToUserType: () -> Unit,
    goToHomeScreen: (String) -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.secondary),
                title = {
                    Text(text = "О Вас", color = Color.White)
                },
                navigationIcon = {
                    IconButton(onClick = {
                        Log.d("FINDME", "UserInfoInputScreen: pressed back button")
                        backToUserType()
                    }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            tint = Color.White,
                            contentDescription = "вернуться на прошлый экран",
                        )
                    }
                },
            )

        }
    ) { paddings ->

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colorScheme.primary)
                .padding(paddings)
                .padding(horizontal = 32.dp),
        ) {
            Spacer(modifier = Modifier.height(64.dp))
            TextArea(label = "Фамилия", value = surname, onValueChange = onSurnameChange)
            Spacer(modifier = Modifier.height(20.dp))
            TextArea(label = "Имя", value = name, onValueChange = onNameChange)
            Spacer(modifier = Modifier.height(20.dp))
            TextArea(label = "Отчество", value = middleName, onValueChange = onMiddleNameChange)
            if (userType == UserTypes.TEACHER) {
                Spacer(modifier = Modifier.height(20.dp))
                TextArea(label = "Должность", value = position, onValueChange = onPositionChanged)
            }

            ElevatedButton(
                modifier = Modifier
                    .padding(top = 64.dp)
                    .align(Alignment.End),
                colors = ButtonDefaults.elevatedButtonColors(containerColor = AccentPink),
                shape = RoundedCornerShape(15.dp),
                elevation = ButtonDefaults.buttonElevation(defaultElevation = 3.dp),
                onClick = { goToHomeScreen(userType) },
            ) {
                Text(
                    text = "Далее",
                    color = Color.White,
                    fontSize = 20.sp
                )
                Icon(
                    modifier = Modifier.size(32.dp),
                    imageVector = Icons.Rounded.KeyboardArrowRight,
                    tint = Color.White,
                    contentDescription = "далее",
                )
            }
        }
    }
}


@Preview(showSystemUi = true)
@Composable
fun UserInfoInputPreview() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        UserInfoInputScreen(
            "teacher",
            "Surname",
            {},
            "Name",
            {},
            "Middle name",
            {},
            "Doljnost",
            {},
            {},
            {}
        )
    }
}
