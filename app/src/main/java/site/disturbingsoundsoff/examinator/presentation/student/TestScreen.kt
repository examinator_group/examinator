package site.disturbingsoundsoff.examinator.presentation.student

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material.icons.rounded.ArrowForward
import androidx.compose.material.icons.rounded.Done
import androidx.compose.material.icons.rounded.KeyboardArrowLeft
import androidx.compose.material.icons.rounded.KeyboardArrowRight
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.data.Question
import site.disturbingsoundsoff.examinator.data.Test
import site.disturbingsoundsoff.examinator.presentation.teacher.test.EditTestBottomBar
import site.disturbingsoundsoff.examinator.presentation.teacher.test.EditTestTopBar
import site.disturbingsoundsoff.examinator.presentation.teacher.test.NavBottomIcon
import site.disturbingsoundsoff.examinator.presentation.ui.theme.AccentPink
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme

@Composable
fun TestScreen(
    test: Test,
    goBack: () -> Unit,
    saveResultToDb: () -> Unit,
    questions: List<Question>,
    currentIndex: Int,
    setCurrentIndex: (Int) -> Unit
) {

    Scaffold(
        topBar = { EditTestTopBar(
            saveToDb = saveResultToDb,
            goBack = goBack,
            title = "Вопрос $currentIndex"
        ) },
        bottomBar = { TestBottomBar(
            questions = questions,
            currentIndex = currentIndex,
            setCurrentIndex = setCurrentIndex
        )}
    ) { paddings ->
        Column(
            Modifier
                .background(MaterialTheme.colorScheme.primary)
                .padding(paddings)
        ) {
            Text(text = questions.toString())
            Spacer(modifier = Modifier.weight(1f))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp, bottom = 16.dp),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                ElevatedButton(
                    colors = ButtonDefaults.elevatedButtonColors(containerColor = AccentPink),
                    shape = RoundedCornerShape(16.dp),
//                    contentPadding = PaddingValues(vertical = 4.dp, horizontal = 16.dp),
                    onClick = { /*TODO*/ }
                ) {
                    Icon(
                        imageVector = Icons.Rounded.KeyboardArrowLeft,
                        contentDescription = "вернуться к предыдущему вопросу",
                        tint = Color.White,
                        modifier = Modifier
                            .padding(end = 10.dp)
                            .size(32.dp)
                    )
                    Text(text = "Назад", color = Color.White)
//                    Spacer(modifier = Modifier.width(16.dp))

                }
                ElevatedButton(
                    colors = ButtonDefaults.elevatedButtonColors(containerColor = AccentPink),
                    shape = RoundedCornerShape(16.dp),
                    onClick = { /*TODO*/ }
                ) {
                    Text(text = "Далее", color = Color.White)
                    Icon(
                        imageVector = Icons.Rounded.KeyboardArrowRight,
                        contentDescription = "перейти к следующему вопросу",
                        tint = Color.White,
                        modifier = Modifier
                            .padding(start = 10.dp)
                            .size(32.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun TestBottomBar(
    questions: List<Question>,
    currentIndex: Int,
    setCurrentIndex: (Int) -> Unit
) {
    LazyRow(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.secondary)
            .padding(bottom = 32.dp, top = 8.dp)
    ) {
        items(items = questions, key = { it.id }) {question ->
            val itemIndex = questions.indexOf(question)
            NavBottomIcon(
                modifier = Modifier.clickable {
                    if (currentIndex != itemIndex) { setCurrentIndex(itemIndex) }
                },
                selected = itemIndex == currentIndex,
            ){
                Text(
                    modifier = Modifier.padding(horizontal = 6.dp),
                    text = (itemIndex + 1).toString(),
                    style = TextStyle(
                        color = if (itemIndex == currentIndex) Color.White
                        else AccentPink
                    )
                )
            }
        }
    }
}

@Preview
@Composable
fun TestScreenPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        val question = Question(
            id = 1,
            body = "open ended question",
            score = 1,
            correctAnswer = "example on open ended answer",
            answers = emptyList(),
            openEnded = true,
        )
        TestScreen(
            test = Test(id = 0, teacherId = 0, author = "oleg", title = "very long test title"),
            goBack = { /*TODO*/ },
            saveResultToDb = { /*TODO*/ },
            questions = listOf(question, question.copy(id=2), question.copy(id=3), question.copy(id=4)),
            currentIndex = 2,
            setCurrentIndex = {}
        )
    }
}