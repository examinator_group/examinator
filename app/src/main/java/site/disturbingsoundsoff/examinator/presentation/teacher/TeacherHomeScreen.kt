package site.disturbingsoundsoff.examinator.presentation.teacher

import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.R
import site.disturbingsoundsoff.examinator.presentation.components.CustomButton
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme
import site.disturbingsoundsoff.examinator.presentation.ui.theme.UnfocusedGray

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TeacherHomeScreen(
    viewTests: () -> Unit,
    createTest: () -> Unit,
    goToSettings: () -> Unit,
    launchFilePicker: () -> Unit
) {

    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.secondary),
                title = {
                    Text(
                        modifier = Modifier.padding(start = 40.dp),
                        text = "Главная",
                        color = Color.White
                    )
                },
            )
        }
    ) { paddings ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colorScheme.primary)
                .padding(paddings)
        ) {

            //TODO: сделай тут список карточек с избранными тестами

            Spacer(modifier = Modifier.height(16.dp))

            Column(
                modifier = Modifier.padding(horizontal = 32.dp),
                verticalArrangement = Arrangement.spacedBy(16.dp)
            ) {

                CustomButton(
                    text = "Создать тест",
                    onClick = createTest
                ) {
                    Icon(
                        imageVector = Icons.Rounded.Add,
                        contentDescription = "Создать тест",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                CustomButton(
//                    text = "Посмотреть тесты",
                    text = stringResource(id = R.string.see_tests),
                    onClick = viewTests
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.file),
                        contentDescription = "Посмотреть тесты",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                // TODO: DEAL WITH THIS SHIT IMMEDIATELY !!!!
                var test by remember { mutableStateOf("not used") }
                val launcher = rememberLauncherForActivityResult(contract = ActivityResultContracts.GetContent()) {
                    test = it?.toString() ?: "nothing"
                }
//                Text(text = test)

                CustomButton(
                    text = "Импортировать тест",
//                    onClick = launchFilePicker
                    onClick = { launcher.launch("application/json") }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.resource_import),
                        contentDescription = "Импортировать тест",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                Divider(
                    modifier = Modifier.clip(CircleShape),
                    color = UnfocusedGray
                )

                CustomButton(
                    text = "Настройки",
                    onClick = goToSettings
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.cool_settings),
                        contentDescription = "Настройки",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                CustomButton(
                    text = "Сервер",
                    onClick = { /*TODO*/ }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.cloud),
                        contentDescription = "Сервер",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

            }
        }
    }
}


@Preview(showSystemUi = true)
@Composable
private fun TeacherHomeScreenPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        TeacherHomeScreen(
            {},
            {},
            {},
            {}
        )
    }
}

    
