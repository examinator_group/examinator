package site.disturbingsoundsoff.examinator.presentation.teacher.test

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import site.disturbingsoundsoff.examinator.data.AnswerRepository
import site.disturbingsoundsoff.examinator.data.QuestionRepository
import site.disturbingsoundsoff.examinator.data.Test
import site.disturbingsoundsoff.examinator.data.TestRepository
import site.disturbingsoundsoff.examinator.data.toQuestion
import site.disturbingsoundsoff.examinator.data.toTest
import java.io.File
import java.io.FileOutputStream

private const val TAG = "TEST_ACTION_VIEW_MODEL"

class TestActionViewModel(
    private val id: Long,
    private val filesDir: File,
    private val testRepository: TestRepository,
    private val questionRepository: QuestionRepository,
    private val answerRepository: AnswerRepository
) : ViewModel() {

    // TODO: you get test and all it's stuff twice, fixit
    private val path = "$filesDir/Tests"

    private val _studentTestFile = MutableStateFlow(File(path))
    val studentTestFile = _studentTestFile.asStateFlow()

    private val _exportTestFile = MutableStateFlow(File(path))
    val exportTestFile = _exportTestFile.asStateFlow()

    init {
        saveDataToJsonFile()
    }

    fun deleteTest() {
        runBlocking {
            val questions = questionRepository
                .getAllQuestionsForTest(id)
                .map { it.toQuestion() }

            for (question in questions){
                answerRepository.deleteAnswersForQuestion(question.id)
                questionRepository.deleteQuestion(question.id)
            }
            testRepository.deleteTest(id)
        }
    }

    private fun saveDataToJsonFile() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){

                // this test will definitely exist, because this screen wouldn't be accessable otherwise
                val test = testRepository.getDbTest(id)!!.toTest()

                val questions = questionRepository
                    .getAllQuestionsForTest(id)
                    .map { it.toQuestion() }
                for (q in questions){
                    q.answers = answerRepository
                        .getAnswersForQuestion(q.id)
                        .map { it.body }
                }

                test.questions = questions

                File(path).mkdirs()

                saveForExport(test)
                saveForStudents(test)
            }

        }
    }

    private suspend fun saveForStudents(
        test: Test,
    ) {
        val tempTest = test.copy()
        tempTest.questions.forEach {
            it.correctAnswer = ""
        }

        val fileName = tempTest.title.replace(" ", "_") + ".json"
        _studentTestFile.update { File(path, fileName) }
        val json = Json.encodeToString(tempTest)

        withContext(Dispatchers.IO) {
            FileOutputStream(_studentTestFile.value).write(json.toByteArray())
        }
    }

    private suspend fun saveForExport(
        test: Test,
    ) {

        val fileName = test.title.replace(" ", "_") + "export.json"
        _exportTestFile.update { File(path, fileName) }
        val json = Json.encodeToString(test)

        withContext(Dispatchers.IO) {
            FileOutputStream(_exportTestFile.value).write(json.toByteArray())
        }
    }

}
