package site.disturbingsoundsoff.examinator.presentation.teacher.test

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.data.Test
import site.disturbingsoundsoff.examinator.presentation.components.TestCard
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ListTestScreen(
    tests: List<Test>,
    goToTestActions: (Long) -> Unit,
    goBack: () -> Unit
) {

    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.secondary
                ),
                title = {
                    Text(text = "Все тесты")
                },
                navigationIcon = {
                    IconButton(
                        onClick = goBack
                    ) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            tint = Color.White,
                            contentDescription = "вернуться на прошлый экран",
                        )
                    }
                },
            )

        }
    ) { paddings ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.primary)
                .padding(paddings)
                .padding(horizontal = 32.dp)
        ) {
            Spacer(modifier = Modifier.height(32.dp))

            LazyColumn {
                items(items = tests, key = { test -> test.id }) { test ->
                    TestCard(
                        modifier = Modifier
                            .clickable { goToTestActions(test.id) }
                            .padding(bottom = 32.dp),
                        title = test.title,
                        subject = test.subject,
                        favourite = false
                    )
                }
            }

        }
    }
}

@Preview
@Composable
private fun ListTestScreenPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        ListTestScreen(
            listOf(
                Test(
                    id = 1,
                    teacherId = 1,
                    title = "Основные законодательные акты Российской Федерации",
                    subject = "Основы безопасности жизнедеятельности",
                    author = "",
                )
            ),
            goToTestActions = {},
            goBack = {},
        )
    }
}
