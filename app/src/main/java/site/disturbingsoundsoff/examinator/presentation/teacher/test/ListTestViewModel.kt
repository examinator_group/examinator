package site.disturbingsoundsoff.examinator.presentation.teacher.test

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import site.disturbingsoundsoff.examinator.data.TestRepository
import site.disturbingsoundsoff.examinator.data.toTest

class ListTestViewModel(
    private val testReposotory: TestRepository
) : ViewModel() {
    // TODO: there is no need in mapping dbtests to regular tests, think about it
    val tests = testReposotory.getAllTests()
        .map { list ->
            list.map { dbTest ->
                dbTest.toTest()
            }
        }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000L),
            initialValue = emptyList(),
        )
}