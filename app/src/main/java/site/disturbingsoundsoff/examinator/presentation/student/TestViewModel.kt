package site.disturbingsoundsoff.examinator.presentation.student

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import site.disturbingsoundsoff.examinator.data.Question
import site.disturbingsoundsoff.examinator.data.Test

private const val TAG = "TestViewModel"
class TestViewModel(
    fileData: String
): ViewModel() {
    private val _test = MutableStateFlow(Test(id=-1, author = "error", teacherId = -1))
    val test = _test.asStateFlow()

    private val _questions = MutableStateFlow(listOf<Question>())
    val questions = _questions.asStateFlow()

    private val _currentIndex = MutableStateFlow(0)
    val currentIndex = _currentIndex.asStateFlow()

    var question: Question by mutableStateOf( Question(id = -1) )
        private set

    init {
        parseFileData(fileData)
    }

    fun setCurrentIndex(newId: Int) {
        _currentIndex.update { newId }
        updateCurrentQuestion()
    }

    private fun updateCurrentQuestion(){
        if (currentIndex.value >= 0){
            question = _test.value.questions[currentIndex.value].copy()
        }
    }

    private fun parseFileData(fileData: String) {

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val test = Json.decodeFromString<Test>(fileData)
                _test.update {
                    test
                }
                _questions.update { test.questions }

            } catch (e: Exception) {
                // TODO: handle error properly
                Log.e(TAG, "parseFile: $e", )
            }
        }
    }
}