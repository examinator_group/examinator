package site.disturbingsoundsoff.examinator.presentation.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.common.UserTypes
import site.disturbingsoundsoff.examinator.presentation.components.CustomCenterButton
import site.disturbingsoundsoff.examinator.presentation.components.TextArea
import site.disturbingsoundsoff.examinator.presentation.ui.theme.AccentPink
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OldSettingScreen(
    userType: String,
    goBack: () -> Unit,

    name: String,
    onNameChange: (String) -> Unit,
    surname: String,
    onSurnameChange: (String) -> Unit,
    middleName: String,
    onMiddleNameChange: (String) -> Unit,
    position: String,
    onPositionChanged: (String) -> Unit,
) {
    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.secondary),
                title = {
                    Text(text = "Настройки", color = Color.White)
                },
                navigationIcon = {
                    IconButton(onClick = goBack) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            tint = Color.White,
                            contentDescription = "вернуться на прошлый экран",
                        )
                    }
                },
            )

        }
    ) { paddings ->

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colorScheme.primary)
                .padding(paddings)
                .padding(horizontal = 32.dp),
        ) {
            Spacer(modifier = Modifier.height(64.dp))
            TextArea(label = "Фамилия", value = surname, onValueChange = onSurnameChange)

            Spacer(modifier = Modifier.height(20.dp))
            TextArea(label = "Имя", value = name, onValueChange = onNameChange)

            Spacer(modifier = Modifier.height(20.dp))
            TextArea(label = "Отчество", value = middleName, onValueChange = onMiddleNameChange)

            if (userType == UserTypes.TEACHER) {
                Spacer(modifier = Modifier.height(20.dp))
                TextArea(label = "Должность", value = position, onValueChange = onPositionChanged)
            }

            Spacer(modifier = Modifier.height(20.dp))
            CustomCenterButton(
                modifier = Modifier.padding(top = 16.dp),
                text = "Сохранить",
                color = AccentPink,
                textColor = Color.White,
                onClick = { /*TODO*/ }
            )

        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun UserInfoInputPreview() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        OldSettingScreen(
            userType = "teacher",
            surname = "Surname",
            onSurnameChange = {},
            name = "Name",
            onNameChange = {},
            middleName = "Middle name",
            onMiddleNameChange = {},
            position = "Doljnost",
            onPositionChanged = {},
            goBack = {},
        )
    }
}
