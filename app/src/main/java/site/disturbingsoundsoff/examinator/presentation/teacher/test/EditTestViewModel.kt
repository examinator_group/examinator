package site.disturbingsoundsoff.examinator.presentation.teacher.test

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import site.disturbingsoundsoff.examinator.data.AnswerRepository
import site.disturbingsoundsoff.examinator.data.Question
import site.disturbingsoundsoff.examinator.data.QuestionRepository
import site.disturbingsoundsoff.examinator.data.Test
import site.disturbingsoundsoff.examinator.data.TestRepository
import site.disturbingsoundsoff.examinator.data.toDbQuestion
import site.disturbingsoundsoff.examinator.data.toDbTest
import site.disturbingsoundsoff.examinator.data.toQuestion
import site.disturbingsoundsoff.examinator.data.toTest

private const val TAG = "EditTestViewModel"

class EditTestViewModel(
    id: Long,
    private val testRepository: TestRepository,
    private val questionRepository: QuestionRepository,
    private val answerRepository: AnswerRepository
) : ViewModel() {

    private val _test = MutableStateFlow(Test(id = 1, teacherId = 1, author = "Ogleg"))
    val test = _test.asStateFlow()
    private val _currentIndex = MutableStateFlow(-1)
    val currentIndex = _currentIndex.asStateFlow()

    var question: Question by mutableStateOf( Question(id = -1) )
        private set

    init {

        viewModelScope.launch {

            if (id > 0) {

                _test.update {
                    testRepository.getDbTest(id)!!.toTest()
                }

                val tempQuestions = questionRepository
                    .getAllQuestionsForTest(id)
                    .map { it.toQuestion() }

                for (q in tempQuestions){
                    q.answers = answerRepository
                        .getAnswersForQuestion(q.id)
                        .map { it.body }
                }

                _test.update { it.copy(
                    questions = tempQuestions
                ) }

            }
            else {
                val latestIndex = testRepository.getLatestTestId() ?: 0
                _test.update { it.copy(id = latestIndex + 1) }
//                addQuestion()
            }

        }
    }

    fun setCurrentIndex(newId: Int) {
        _currentIndex.update { newId }
        updateCurrentQuestion()
    }

    fun setCorrectAnswer(newAnswer: String){
        val questions = _test.value.questions
        questions[currentIndex.value].correctAnswer = newAnswer

        _test.update { it.copy(
            questions = questions
        ) }

        updateCurrentQuestion()
    }

    fun setQuestionBody(newBody: String){

            val questions = _test.value.questions
            questions[currentIndex.value].body = newBody

            _test.update { it.copy(
                questions = questions
            ) }

            updateCurrentQuestion()
    }

    fun onTitleChanged(newTitle: String) {
        _test.update { it.copy(title = newTitle) }
    }

    fun onSubjectChanged(newSubject: String) {
        _test.update { it.copy(subject = newSubject) }
    }

    fun addQuestion(){
        var newQuestionId: Long = 1

        runBlocking {
            val dbId = questionRepository.getLatestQuestionId() ?: 0
            val testId = if (_test.value.questions.isNotEmpty())
                _test.value.questions.last().id else 0
            newQuestionId += if ( testId > dbId) testId else dbId
        }

        val newQuestion = Question(
            id = newQuestionId,
            body = "question$newQuestionId",
            score = 1,
            correctAnswer = "tt"
        )

        _test.update { it.copy(
            questions = it.questions + newQuestion
        ) }

//        setCurrentIndex(_test.value.questions.indexOf(newQuestion))

    }

    fun setScore(newScore: Int){
        val questions = _test.value.questions
        questions[currentIndex.value].score = newScore

        _test.update { it.copy(
            questions = questions
        ) }

        updateCurrentQuestion()

    }

    fun makeQuestionOpenEnded(option: Boolean = true) {
        val questions = _test.value.questions
        questions[currentIndex.value].openEnded = option
        questions[currentIndex.value].correctAnswer = ""

        _test.update { it.copy(
            questions = questions
        ) }
        updateCurrentQuestion()
    }
    
    // TODO: add option to quickly make yes/no true/false question

    fun addAnswer() {
        val questions = _test.value.questions
        questions[currentIndex.value].answers += "ответ ${questions[currentIndex.value].answers.lastIndex + 2}"

        _test.update { it.copy(
            questions = questions
        ) }

        updateCurrentQuestion()
    }

    fun setAnswer(index: Int, newAnswer: String) {
        val questions = _test.value.questions
        val answers = questions[currentIndex.value].answers.toMutableList()

        answers[index] = newAnswer
        questions[currentIndex.value].answers = answers.toList()

        _test.update { it.copy(
            questions = questions
        ) }

        updateCurrentQuestion()
    }

    fun deleteAnswer(index: Int) {
        val questions = _test.value.questions
        val answers = questions[currentIndex.value].answers.toMutableList()

        val changeCorrectAnswer = answers[index] == questions[currentIndex.value].correctAnswer

        answers.removeAt(index)

        if (changeCorrectAnswer){
            setCorrectAnswer(answers.firstOrNull() ?: "")
        }

        questions[currentIndex.value].answers = answers

        _test.update { it.copy(
            questions = questions
        ) }

        updateCurrentQuestion()
    }

    fun deleteOpenEndedAnswer() {
        setCorrectAnswer("")
        makeQuestionOpenEnded(false)
    }

    fun deleteQuestion(id: Long) {

        setCurrentIndex(currentIndex.value - 1)

        val questions = _test.value.questions.toMutableList()
        questions.removeIf{ question -> question.id == id}
        _test.update { it.copy(
            questions = questions
        ) }

        updateCurrentQuestion()


        viewModelScope.launch {
            answerRepository.deleteAnswersForQuestion(id)
            questionRepository.deleteQuestion(id)
        }
    }

    private fun updateCurrentQuestion(){
//        _question.update { _test.value.questions.getOrNull(currentIndex.value) }
        if (currentIndex.value >= 0){
            question = _test.value.questions[currentIndex.value].copy()
        }
    }

    fun saveTest() {
        // TODO: запретить сохранение теста если название и предмет пустые
        // TODO: или если вопросы/ответы пустые есть

        // TODO: This needs some time to save all questions, so i can't navigate back right after saving

        viewModelScope.launch {
            for (question in test.value.questions) {

                answerRepository.deleteAnswersForQuestion(question.id)
                for (answer in question.answers) {
                    answerRepository.insertAnswer(question.id, answer)
                }

                questionRepository.insertQuestion(question.toDbQuestion(test.value.id))
            }

            testRepository.insertTest(test.value.toDbTest())
        }

    }

    fun parseFileData(fileContents: String) {
        Log.d(TAG, "setTestFromFile file contents: $fileContents")
        try {
            _test.update { Json.decodeFromString<Test>(fileContents).copy(id = _test.value.id)}
//            _test.update { it.copy(id = currId) }
        } catch (e: Exception) {
            // TODO: Show some modal window that says "something's wrong with a file" or smth
            Log.e(TAG, "setTestFromFile: $e")
        }
    }
}
