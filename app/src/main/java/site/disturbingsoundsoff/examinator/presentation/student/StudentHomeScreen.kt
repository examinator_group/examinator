package site.disturbingsoundsoff.examinator.presentation.student

import android.content.Context
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.R
import site.disturbingsoundsoff.examinator.presentation.components.CustomButton
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme
import site.disturbingsoundsoff.examinator.presentation.ui.theme.UnfocusedGray
import java.io.InputStream


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StudentHomeScreen(
    goToSettings: () -> Unit,
    goToTest: (String) -> Unit,
    launchFilePicker: () -> Unit,
) {

    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.secondary),
                title = {
                    Text(
                        modifier = Modifier.padding(start = 40.dp),
                        text = "Главная",
                        color = Color.White
                    )
                },
            )

        }
    ) { paddings ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colorScheme.primary)
                .padding(paddings)
                .padding(horizontal = 32.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {

            Spacer(modifier = Modifier.size(0.dp))

            val context = LocalContext.current
            val launcher = rememberLauncherForActivityResult(contract = ActivityResultContracts.GetContent()) { uri ->
                var fileData = ""
                var inputStream: InputStream? = null
                if (uri != null) {
                    try {
                        inputStream = context.contentResolver.openInputStream(uri)
                        fileData = inputStream?.bufferedReader()?.readText() ?: ""
                    } catch (e: Exception) {
                        // TODO: handle error properly
                        Log.e("STUDENT HOME", "StudentHomeScreen: $e")
                    } finally {
                        inputStream?.close()
                    }
                }
                goToTest(fileData)
            }

            CustomButton(
                text = "Открыть тест",
                // onClick = { launcher.launch() }
                onClick = { launcher.launch("application/json") }
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.file),
                    contentDescription = "Открыть тест",
                    tint = MaterialTheme.colorScheme.primary
                )
            }

            CustomButton(
                text = "Тесты онлайн",
                onClick = { /* TODO */ }
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.planet),
                    contentDescription = "Тесты онлайн",
                    tint = MaterialTheme.colorScheme.primary
                )
            }

            Divider(
                modifier = Modifier.clip(CircleShape),
                color = UnfocusedGray
            )

            CustomButton(
                text = "Настройки",
                onClick = goToSettings
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.cool_settings),
                    contentDescription = "Настройки",
                    tint = MaterialTheme.colorScheme.primary
                )
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun TeacherHomeScreenPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        StudentHomeScreen(
            {},
            {},
            {}
        )
    }
}
