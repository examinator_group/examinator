package site.disturbingsoundsoff.examinator.presentation

import  android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.SystemBarStyle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts.GetContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.koin.android.ext.android.get
import site.disturbingsoundsoff.examinator.data.UserDatastoreRepository
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme
import java.io.InputStream


val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")



class MainActivity : ComponentActivity() {

    var uri: Uri? = null
    val getContent = registerForActivityResult(GetContent()) {
        uri = it
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge(
            statusBarStyle = SystemBarStyle.light(
                Color.TRANSPARENT, Color.TRANSPARENT
            ),
            navigationBarStyle = SystemBarStyle.light(
                Color.TRANSPARENT, Color.TRANSPARENT
            )
        )
        super.onCreate(savedInstanceState)

        val datastore: UserDatastoreRepository = get()
        var userType = ""
        runBlocking {
            userType = datastore.userType.first()
        }

        /* TODO: нужно еще чекать заполнил ли юзер о себе данные,
        иначе тоже надо на login его отправлять
        или можно дать юзеру в настройках потом написать
         */

        val filePath = uri ?: intent.data
        var fileContents = ""
        var fileSupported = false
        var fileForStudent = false

        if (filePath != null) {
            var inputStream: InputStream? = null
            try {
                inputStream = contentResolver.openInputStream(filePath)
                fileContents = inputStream?.bufferedReader()?.readText() ?: ""
                fileSupported = Regex("teacherId").containsMatchIn(fileContents)
                fileForStudent = Regex("correctAnswer\":\"\"").containsMatchIn(fileContents)
            } catch (e: Exception) {
                Log.e("OPENED", "onCreate: ERROR $e ")
            } finally {
                inputStream?.close()
            }
        }


        setContent {
            ExaminatorTheme(dynamicColor = false, darkTheme = true) {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    AppNavigation(
                        userTypeDS = userType,
                        fileContents = fileContents,
                        fileForStudent = fileForStudent,
                        fileSupported = fileSupported,
                        launchFilePicker = { getContent.launch("application/json") }
                    )
                }
            }
        }
    }

}
