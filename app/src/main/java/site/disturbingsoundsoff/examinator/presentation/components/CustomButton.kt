package site.disturbingsoundsoff.examinator.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.disturbingsoundsoff.examinator.R
import site.disturbingsoundsoff.examinator.presentation.ui.theme.AccentPink
import site.disturbingsoundsoff.examinator.presentation.ui.theme.ExaminatorTheme

@Composable
fun CustomButton(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    color: Color = Color.White,
    textColor: Color = MaterialTheme.colorScheme.primary,
    paddingValues: PaddingValues = PaddingValues(),
    icon: @Composable() (() -> Unit)? = null,
) {
    ElevatedButton(
        modifier = modifier
            .fillMaxWidth()
            .padding(paddingValues),
        colors = ButtonDefaults.buttonColors(containerColor = color),
        shape = RoundedCornerShape(15.dp),
        onClick = onClick,
        elevation = ButtonDefaults.buttonElevation(defaultElevation = 3.dp),
    ) {
        if (icon != null){
            icon()
            Spacer(modifier = Modifier.width(22.dp))
        }
        Text(
            text = text,
            color = textColor,
        )
        Spacer(modifier = Modifier.weight(1f))
    }

}

@Composable
fun SmallButton(
    onClick: () -> Unit,
    icon: @Composable () -> Unit,
) {
    Box(
        modifier = Modifier
            .padding(horizontal = 8.dp)
            .size(46.dp)
            .shadow(elevation = 8.dp, shape = RoundedCornerShape(16.dp))
            .clip(RoundedCornerShape(16.dp))
            .background(AccentPink)
            .clickable { onClick() }
        ,
        contentAlignment = Alignment.Center
    ) {
        icon()
    }

}

@Composable
fun CustomCenterButton(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    color: Color = Color.White,
    textColor: Color = MaterialTheme.colorScheme.primary,
    paddingValues: PaddingValues = PaddingValues(),
) {
    ElevatedButton(
        modifier = modifier
            .fillMaxWidth()
            .padding(paddingValues)
            .padding(horizontal = 32.dp),
        colors = ButtonDefaults.buttonColors(containerColor = color),
        shape = RoundedCornerShape(15.dp),
        onClick = onClick,
        elevation = ButtonDefaults.buttonElevation(defaultElevation = 3.dp),
    ) {
        Spacer(modifier = Modifier.weight(1f))
        Text(
            text = text,
            color = textColor,
        )
        Spacer(modifier = Modifier.weight(1f))
    }

}

@Preview(showSystemUi = true)
@Composable
private fun CustomButtonPrev() {
    ExaminatorTheme(dynamicColor = false, darkTheme = true) {
        Column(
            Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.primary)
                .padding(horizontal = 32.dp)
        ) {
            CustomButton(text = "Test text long", onClick = {}) {
                Icon(
                    imageVector = Icons.Rounded.Add,
                    contentDescription = "Создать тест",
                    tint = MaterialTheme.colorScheme.primary
                )
            }
            CustomButton(text = "Test text long", onClick = { /*TODO*/ })
            CustomCenterButton(
                text = "Test text long",
                onClick = {},
                color = AccentPink,
                textColor = Color.White
            )
            CustomCenterButton(text = "Test text long", onClick = {})
            SmallButton(onClick = {}) {
                Icon(
                    modifier = Modifier.size(26.dp),
                    painter = painterResource(id = R.drawable.cool_delete),
                    contentDescription = "удалить вопрос",
                    tint = Color.White
                )
            }
        }
    }
}
