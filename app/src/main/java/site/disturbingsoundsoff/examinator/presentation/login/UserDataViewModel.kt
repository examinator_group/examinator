package site.disturbingsoundsoff.examinator.presentation.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import site.disturbingsoundsoff.examinator.data.UserDatastoreRepository

class UserDataViewModel(
    private val userDatastoreRepository: UserDatastoreRepository
) : ViewModel() {
    private val _loginData = MutableStateFlow(LoginData())
    val loginData = _loginData.combine(userDatastoreRepository.userType) { data, type ->
        LoginData(
            name = data.name,
            surname = data.surname,
            middleName = data.middleName,
            position = data.position,
            userType = type
        )
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000L),
        initialValue = LoginData()
    )

    fun setName(newName: String) {
        _loginData.update {
            it.copy(
                name = newName
            )
        }
    }

    fun setSurname(newSurname: String) {
        _loginData.update {
            it.copy(
                surname = newSurname
            )
        }
    }

    fun setMiddlename(newMiddlename: String) {
        _loginData.update {
            it.copy(
                middleName = newMiddlename
            )
        }
    }

    fun setPosition(newPosition: String) {
        _loginData.update {
            it.copy(
                position = newPosition
            )
        }
    }


}

data class LoginData(
    val userType: String = "",
    val name: String = "",
    val surname: String = "",
    val middleName: String = "",
    val position: String = ""
)