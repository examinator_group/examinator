package site.disturbingsoundsoff.examinator.di

import app.cash.sqldelight.driver.android.AndroidSqliteDriver
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import site.disturbingsoundsoff.examinator.ExaminatorDatabase
import site.disturbingsoundsoff.examinator.data.AnswerRepository
import site.disturbingsoundsoff.examinator.data.AnswerRepositoryImpl
import site.disturbingsoundsoff.examinator.data.QuestionRepository
import site.disturbingsoundsoff.examinator.data.QuestionRepositoryImpl
import site.disturbingsoundsoff.examinator.data.TestRepository
import site.disturbingsoundsoff.examinator.data.TestRepositoryImpl
import site.disturbingsoundsoff.examinator.data.UserDatastoreRepository
import site.disturbingsoundsoff.examinator.presentation.login.UserDataViewModel
import site.disturbingsoundsoff.examinator.presentation.login.UserTypeViewModel
import site.disturbingsoundsoff.examinator.presentation.settings.SettingsViewModel
import site.disturbingsoundsoff.examinator.presentation.student.TestViewModel
import site.disturbingsoundsoff.examinator.presentation.teacher.test.EditTestViewModel
import site.disturbingsoundsoff.examinator.presentation.teacher.test.ListTestViewModel
import site.disturbingsoundsoff.examinator.presentation.teacher.test.TestActionViewModel

val appModule = module {

    single<AndroidSqliteDriver> {
        AndroidSqliteDriver(
            schema = ExaminatorDatabase.Schema,
            context = androidContext(),
            name = "examinator.db"
        )
    }

    single<ExaminatorDatabase> {
        ExaminatorDatabase(get<AndroidSqliteDriver>())
    }

    single<AnswerRepository> {
        AnswerRepositoryImpl(
            get<ExaminatorDatabase>()
        )
    }

    single<QuestionRepository> {
        QuestionRepositoryImpl(
            get<ExaminatorDatabase>()
        )
    }

    single<TestRepository> {
        TestRepositoryImpl(
            get<ExaminatorDatabase>(),
        )
    }

    singleOf(::UserDatastoreRepository)

    viewModelOf(::UserTypeViewModel)
    viewModelOf(::UserDataViewModel)
    viewModelOf(::ListTestViewModel)
    viewModelOf(::EditTestViewModel)
    viewModelOf(::TestActionViewModel)
    viewModelOf(::SettingsViewModel)
    viewModelOf(::TestViewModel)
//    viewModel { parameters -> EditTestViewModel(id = parameters.get(), get()) }
}