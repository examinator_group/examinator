package site.disturbingsoundsoff.examinator

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import site.disturbingsoundsoff.examinator.di.appModule

class ExaminatorApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@ExaminatorApplication)
            modules(appModule)
        }
    }
}