package site.disturbingsoundsoff.examinator.data

import app.cash.sqldelight.coroutines.asFlow
import app.cash.sqldelight.coroutines.mapToList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import site.disturbingsoundsoff.examinator.DbQuestion
import site.disturbingsoundsoff.examinator.ExaminatorDatabase

class QuestionRepositoryImpl(db: ExaminatorDatabase): QuestionRepository {

    private val questionQueries = db.questionDbQueries

    override suspend fun getQuestion(id: Long): DbQuestion? {
        return withContext(Dispatchers.IO) {
            questionQueries
                .getQuestion(id)
                .executeAsOneOrNull()
        }
    }

    override fun getAllQuestions(): Flow<List<DbQuestion>> {
        return questionQueries
            .getAllQuestions()
            .asFlow()
            .mapToList(context = Dispatchers.IO)
    }


    override fun getAllQuestionsForTestAsFlow(id: Long): Flow<List<DbQuestion>> {
        return questionQueries
            .getQuestionsForTest(id)
            .asFlow()
            .mapToList(context = Dispatchers.IO)
    }

    override suspend fun getAllQuestionsForTest(id: Long): List<DbQuestion> {
        return withContext(Dispatchers.IO) {
            questionQueries
                .getQuestionsForTest(id)
                .executeAsList()
        }
    }

    override suspend fun getLatestQuestionId(): Long? {
        return withContext(Dispatchers.IO) {
            questionQueries
                .getLatestQuestionId()
                .executeAsOneOrNull()
        }
    }

    override suspend fun deleteQuestion(id: Long) {
        withContext(Dispatchers.IO){
            questionQueries.deleteQuestion(id)
        }
    }

    override suspend fun insertQuestion(question: DbQuestion) {
        withContext(Dispatchers.IO){

            questionQueries.insertQuestion(
                id = question.id,
                test_id = question.test_id,
                body = question.body,
                score = question.score,
                correct_answer = question.correct_answer,
                open_ended = question.open_ended,
            )

        }
    }
}
