package site.disturbingsoundsoff.examinator.data

import app.cash.sqldelight.coroutines.asFlow
import app.cash.sqldelight.coroutines.mapToList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import site.disturbingsoundsoff.examinator.DbAnswer
import site.disturbingsoundsoff.examinator.ExaminatorDatabase

class AnswerRepositoryImpl(db: ExaminatorDatabase) : AnswerRepository {

    private val queries = db.answerDbQueries

    override suspend fun getAnswer(id: Long): DbAnswer? {
        return withContext(Dispatchers.IO){
            queries.getAnswer(id).executeAsOneOrNull()
        }
    }

    override fun getAllAnswers(): Flow<List<DbAnswer>> {
        return queries
            .getAllAnswers()
            .asFlow()
            .mapToList(Dispatchers.IO)
    }

    override fun getAnswersForQuestionAsFlow(id: Long): Flow<List<DbAnswer>> {
        return queries
            .getAnswersForQuestion(id)
            .asFlow()
            .mapToList(Dispatchers.IO)
    }

    override suspend fun getAnswersForQuestion(id: Long): List<DbAnswer> {
        return withContext(Dispatchers.IO) {
            queries
                .getAnswersForQuestion(id)
                .executeAsList()
        }
    }

    override suspend fun deleteAnswer(id: Long) {
        withContext(Dispatchers.IO){
            queries.deleteAnswer(id)
        }
    }

    override suspend fun deleteAnswersForQuestion(questionId: Long) {
        withContext(Dispatchers.IO){
            queries.deleteAllAnswersForQuestion(questionId)
        }
    }

    override suspend fun insertAnswer(questionId: Long, body: String) {
        withContext(Dispatchers.IO){
            queries.insertAnswer(questionId, body)
        }
    }

}