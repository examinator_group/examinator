package site.disturbingsoundsoff.examinator.data

import app.cash.sqldelight.coroutines.asFlow
import app.cash.sqldelight.coroutines.mapToList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import site.disturbingsoundsoff.examinator.DbTest
import site.disturbingsoundsoff.examinator.ExaminatorDatabase

private const val TAG = "TEST_REPOSITORY_IMPL"

class TestRepositoryImpl(
    db: ExaminatorDatabase,
) : TestRepository {

    private val testQueries = db.testDbQueries


    override suspend fun getDbTest(id: Long): DbTest? {
        return withContext(Dispatchers.IO) {
            testQueries.getTest(id).executeAsOneOrNull()
        }
    }

    override fun getAllTests(): Flow<List<DbTest>> {
        return testQueries.getAllTests().asFlow().mapToList(context = Dispatchers.IO)
    }

    override suspend fun getLatestTestId(): Long? {
        return testQueries.getLatestTestId().executeAsOneOrNull()
    }

    override suspend fun deleteTest(id: Long) {
        withContext(Dispatchers.IO) {
            testQueries.deleteTest(id)
        }
    }

    override suspend fun insertTest(test: DbTest) {
        withContext(Dispatchers.IO) {

            testQueries.insertTest(
                id = test.id,
                title = test.title,
                subject = test.subject,
                author = test.author,
                teacher_id = test.teacher_id,
                due_date = test.due_date,
                favourite = test.favourite
            )

        }
    }
}