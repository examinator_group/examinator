package site.disturbingsoundsoff.examinator.data

data class Result(
    val id: Long,
    val testId: Long,
    val testName: String,
    val studentName: String,
    val studentGroup: String,
    val answers: List<String>,
)
