package site.disturbingsoundsoff.examinator.data

import kotlinx.coroutines.flow.Flow
import site.disturbingsoundsoff.examinator.DbTest

interface TestRepository {

    suspend fun getDbTest(id: Long): DbTest?

    fun getAllTests(): Flow<List<DbTest>>

    suspend fun getLatestTestId(): Long?

    suspend fun deleteTest(id: Long)

    suspend fun insertTest(test: DbTest)
}