package site.disturbingsoundsoff.examinator.data

import kotlinx.serialization.Serializable
import site.disturbingsoundsoff.examinator.DbTest

@Serializable
data class Test(
    val id: Long,
    val teacherId: Long,
    val title: String = "",
    val subject: String = "",
    val author: String,
    var questions: List<Question> = emptyList(),
    val dueDate: String? = null,
    val favourite: Boolean = false,
)

fun Test.toDbTest(): DbTest = DbTest(
    id = id,
    teacher_id = teacherId,
    title = title,
    subject = subject,
    author = author,
    due_date = dueDate,
    favourite = if (favourite) 1 else 0
)

fun DbTest.toTest(): Test = Test(
    id = id,
    teacherId = teacher_id,
    title = title,
    subject = subject,
    author = author,
    dueDate = due_date,
    favourite = if (favourite == null || favourite < 1) false else true
)
