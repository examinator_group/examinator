package site.disturbingsoundsoff.examinator.data

import kotlinx.coroutines.flow.Flow
import site.disturbingsoundsoff.examinator.DbAnswer

interface AnswerRepository {

    suspend fun getAnswer(id: Long): DbAnswer?

    fun getAllAnswers(): Flow<List<DbAnswer>>

    fun getAnswersForQuestionAsFlow(id: Long): Flow<List<DbAnswer>>

    suspend fun getAnswersForQuestion(id: Long): List<DbAnswer>

    suspend fun deleteAnswer(id: Long)

    suspend fun deleteAnswersForQuestion(questionId: Long)

    suspend fun insertAnswer(questionId: Long, body: String)

}
