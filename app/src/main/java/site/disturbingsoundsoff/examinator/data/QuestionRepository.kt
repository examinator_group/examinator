package site.disturbingsoundsoff.examinator.data

import kotlinx.coroutines.flow.Flow
import site.disturbingsoundsoff.examinator.DbQuestion

interface QuestionRepository {

    suspend fun getQuestion(id: Long): DbQuestion?

    fun getAllQuestions(): Flow<List<DbQuestion>>

    fun getAllQuestionsForTestAsFlow(id: Long): Flow<List<DbQuestion>>

    suspend fun getAllQuestionsForTest(id: Long): List<DbQuestion>

    suspend fun getLatestQuestionId(): Long?

    suspend fun deleteQuestion(id: Long)

    suspend fun insertQuestion(question: DbQuestion)
}
