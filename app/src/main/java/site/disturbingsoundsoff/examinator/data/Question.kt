package site.disturbingsoundsoff.examinator.data

import kotlinx.serialization.Serializable
import site.disturbingsoundsoff.examinator.DbQuestion

@Serializable
data class Question(
    val id: Long,
    var body: String = "вопрос",
    var score: Int = 1,
    var answers: List<String> = emptyList(),
    var openEnded: Boolean = false,
    var correctAnswer: String = "ответ"
)
fun Question.toDbQuestion(testId: Long): DbQuestion = DbQuestion(
    id = id,
    test_id = testId,
    body = body,
    score = score.toLong(),
    correct_answer = correctAnswer,
    open_ended = if (openEnded) 1 else 0
)

fun DbQuestion.toQuestion(answers: List<String> = emptyList()): Question = Question(
    id = id,
    body = body,
    score = score.toInt(),
    correctAnswer = correct_answer,
    answers = answers,
    openEnded = open_ended == 1.toLong()
)
