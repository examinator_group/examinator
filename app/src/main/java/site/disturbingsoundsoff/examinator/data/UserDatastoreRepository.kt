package site.disturbingsoundsoff.examinator.data

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import site.disturbingsoundsoff.examinator.common.DataStoreKeys
import site.disturbingsoundsoff.examinator.common.UserTypes
import java.io.IOException

private const val TAG = "USER_DATASTORE_REPOSITORY"

val Context.datastore: DataStore<Preferences> by preferencesDataStore(
    name = DataStoreKeys.DATASTORE_NAME
)

class UserDatastoreRepository(
//    private val datastore: DataStore<Preferences>
    private val context: Context
) {

    //    private val Context.datastore : DataStore<Preferences> by preferencesDataStore(
    //        name = DataStoreKeys.DATASTORE_NAME
    //    )
    private val datastore = context.datastore
    val userType: Flow<String> = datastore.getStringData(DataStoreKeys.USER_TYPE_KEY)


    suspend fun setUserType(newUserType: String) {
//        Log.d(TAG, "setUserType: $newUserType")
        datastore.edit { preferences ->
//        context.datastore.edit {preferences ->
            preferences[DataStoreKeys.USER_TYPE_KEY] = newUserType
        }
    }

}

fun DataStore<Preferences>.getStringData(key: Preferences.Key<String>): Flow<String> {
    return data.catch {
        if (it is IOException) {
            Log.e(TAG, "Error reading user type preference with key: ${key.name}")
            emit(emptyPreferences())
        } else {
            throw it
        }
    }.map { preferences ->
//            Log.d(TAG, "getStringData: ${preferences[key]}")
            preferences[key] ?: UserTypes.NEEDS_LOGIN
    }
}