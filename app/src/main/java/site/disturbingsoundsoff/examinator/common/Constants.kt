package site.disturbingsoundsoff.examinator.common

import androidx.datastore.preferences.core.stringPreferencesKey

object DataStoreKeys {
    const val DATASTORE_NAME: String = "user_data_store"
    val USER_TYPE_KEY = stringPreferencesKey("user_type")
}

object UserTypes {

    const val TEACHER = "teacher"
    const val STUDENT = "student"
    const val NEEDS_LOGIN = "unknown"

    operator fun contains(type: String): Boolean {
        return when(type) {
            TEACHER -> true
            STUDENT -> true
            NEEDS_LOGIN -> true
            else -> false
        }
    }
}

const val PICK_JSON_FILE = 1
