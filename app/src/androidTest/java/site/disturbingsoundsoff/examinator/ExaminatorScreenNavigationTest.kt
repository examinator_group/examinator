package site.disturbingsoundsoff.examinator

import android.util.Log
import androidx.activity.ComponentActivity
import androidx.annotation.StringRes
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.SemanticsNodeInteraction
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.NavController
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.testing.TestNavHostController
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.Assert.assertEquals
import site.disturbingsoundsoff.examinator.common.UserTypes
import site.disturbingsoundsoff.examinator.presentation.AppNavigation

const val TAG = "NAVIGATION_TEST"

fun <A : ComponentActivity> AndroidComposeTestRule<ActivityScenarioRule<A>, A>.onNodeWithStringId(
    @StringRes id: Int
): SemanticsNodeInteraction = onNodeWithText(activity.getString(id))

fun NavController.assertCurrentRouteName(expectedRouteName: String) {
    assertEquals(expectedRouteName, currentBackStackEntry?.destination?.route)
}

class ExaminatorScreenNavigationTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    private lateinit var navController: TestNavHostController

    @Before
    fun setupNavHostWithTeacher() {
        composeTestRule.setContent {
            navController = TestNavHostController(LocalContext.current).apply {
                navigatorProvider.addNavigator(ComposeNavigator())
            }
            AppNavigation(userTypeDS = UserTypes.TEACHER, navController = navController)
        }
    }

    @Test
    fun verifyStartDestination() {
        Log.d(TAG, "verifyStartDestination: ${navController.currentBackStackEntry?.destination?.route}")
        navController.assertCurrentRouteName("teacher_home")
    }

    @Test
    fun verifyBackNavigationNotShownOnHomeScreen() {
        val backText = composeTestRule.activity.getString(R.string.back_button)
        composeTestRule.onNodeWithContentDescription(backText).assertDoesNotExist()
    }

    @Test
    fun clickSeeTestsNavigatesToListTestScreen() {
        composeTestRule
            .onNodeWithStringId(
                R.string.see_tests
            )
            .performClick()
        navController
            .assertCurrentRouteName("list_tests")
    }

}