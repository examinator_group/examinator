import com.android.build.gradle.api.BaseVariantOutput
import com.android.build.gradle.internal.scope.ProjectInfo.Companion.getBaseName
import org.jetbrains.kotlin.gradle.plugin.mpp.pm20.util.archivesName

@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.kotlinAndroid)

    // MINE
    alias(libs.plugins.sqlDelight)
    kotlin("plugin.serialization") version "1.8.10"
}

android {
    namespace = "site.disturbingsoundsoff.examinator"
    compileSdk = 34


    defaultConfig {
        applicationId = "site.disturbingsoundsoff.examinator"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        archivesName = "Examinator"
        applicationIdSuffix = ""
        versionNameSuffix = ""


        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            applicationIdSuffix = ""
            versionNameSuffix = ""
            setProperty("archivesBaseName", "Examinator")
            signingConfig = signingConfigs.getByName("debug")

        }
        debug {
            applicationIdSuffix = ""
            versionNameSuffix = ""
            setProperty("archivesBaseName", "Examinator")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"

        // i use compatible kotin version (1.8.10), but sqldelight installed 1.8.22 and i think it triggers an error
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    implementation(libs.core.ktx)
    implementation(libs.lifecycle.runtime.ktx)
    implementation(libs.activity.compose)
    implementation(platform(libs.compose.bom))
    implementation(libs.ui)
    implementation(libs.ui.graphics)
    implementation(libs.ui.tooling.preview)
    implementation(libs.material3)

    // MINE
    implementation(libs.compose.navigation)
    implementation(libs.koin.compose)
    implementation(libs.datastore.preferences)
    implementation(libs.sqldelight.andriod.driver)
    implementation(libs.sqldelight.coroutines)
    implementation(libs.kotlinx.serialization)



    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.test.ext.junit)
    androidTestImplementation(libs.espresso.core)
    androidTestImplementation(platform(libs.compose.bom))
    androidTestImplementation(libs.ui.test.junit4)
    debugImplementation(libs.ui.tooling)
    debugImplementation(libs.ui.test.manifest)

    // MINE
    androidTestImplementation(libs.nav.testing)
}

sqldelight {
    databases {
        create("ExaminatorDatabase") {
            packageName.set("site.disturbingsoundsoff.examinator")
        }
    }
}
