// Top-level build file where you can add configuration options common to all sub-projects/modules.
@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.kotlinAndroid) apply false

    //MINE
    alias(libs.plugins.sqlDelight) apply false
}
true // Needed to make the Suppress annotation work for the plugins block

// had to add this because sqldelight uses kotlin version, that isn't compatible with compose compiler
buildscript {
    configurations.all {
        resolutionStrategy {
            force("org.jetbrains.kotlin:kotlin-gradle-plugin:1.8.10")
        }
    }
}
